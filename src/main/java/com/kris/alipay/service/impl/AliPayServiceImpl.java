package com.kris.alipay.service.impl;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.kris.alipay.service.AliPayService;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class AliPayServiceImpl implements AliPayService {
    @Override
    public void createAliPrepay(HttpServletRequest request, HttpServletResponse response, Map<String, String> map) throws IOException {



        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", map.get("app_id"), map.get("merchant_private_key"), map.get("format"), map.get("charset"),map.get("alipay_public_key"),map.get("sign_type")); //获得初始化的AlipayClient
        //用户根据实际情况  此处直接填写serverUrl即可
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
        alipayRequest.setReturnUrl(map.get("return_url"));
        alipayRequest.setNotifyUrl(map.get("notify_url"));//在公共参数中设置回跳和通知地址

        //此处数值应该为用户自己的请求传过来的数值 ，此处为测试方便，直接填写静态数据  用户可自行修改

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ map.get("out_trade_no") +"\","
                + "\"total_amount\":\""+ map.get("total_amount") +"\","
                + "\"subject\":\""+ map.get("subject") +"\","
                + "\"body\":\""+ map.get("body") +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");//填充业务参数
        String form="";
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            System.out.println(form);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(form);//直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();


    }

    @Override
    public Map<String, String> notifyAliPrepay(HttpServletRequest request, HttpServletResponse response, String alipay_public_key, String charset, String sign_type) throws IOException, AlipayApiException {
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            //      valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }


        boolean signVerified = AlipaySignature.rsaCheckV1(params, alipay_public_key, charset, sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——

        Map<String, String> map = new HashMap<String, String>();

        System.out.println(signVerified + "ssss");
        if (signVerified) {//验证成功

            Enumeration em = request.getParameterNames();
            while (em.hasMoreElements()) {
                String name = (String) em.nextElement();
                String value = (String) request.getParameter(name);
                map.put(name, value);

            }
            return map;
        }

        return map;
    }

    /* 实际验证过程建议商户务必添加以下校验：
    1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
    2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
    3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
    4、验证app_id是否为该商户本身。
    */
    public void responseWeChatPay(HttpServletResponse response,boolean status) throws IOException {
        if(status){
            response.getWriter().write("success");
            response.getWriter().flush();
            response.getWriter().close();
        }else {
            response.getWriter().write("fail");
            response.getWriter().flush();
            response.getWriter().close();
        }


    }

    @Override
    public Map<String, String> refundAliPay() {
        return null;
    }

}

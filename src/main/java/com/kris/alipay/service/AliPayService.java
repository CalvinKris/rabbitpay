package com.kris.alipay.service;

import com.alipay.api.AlipayApiException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public interface AliPayService {

     void createAliPrepay(HttpServletRequest request, HttpServletResponse response, Map<String, String> map) throws IOException;
     Map<String, String> notifyAliPrepay(HttpServletRequest request, HttpServletResponse response, String alipay_public_key, String charset, String sign_type) throws IOException, AlipayApiException;
     void responseWeChatPay(HttpServletResponse response,boolean status) throws IOException;
     Map<String,String> refundAliPay();
}

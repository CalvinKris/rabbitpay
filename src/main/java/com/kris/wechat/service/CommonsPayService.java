package com.kris.wechat.service;

import org.dom4j.DocumentException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;

/**
 * 功能描述    :
 * 业务描述    :
 * 作 者 名    :@Kris
 * 开发日期    :2018/9/27 14:30
 * Created     :IntelliJ IDEA
 * 修改日期    :
 * 修 改 者    :
 * 修改内容    :
 * **************************************************************
 */
public interface CommonsPayService {
    String CreateSign(SortedMap<String,String> sortedMap, String key);
    Map<String, String> postExecute(String req, String url);
    Map<String, String> resopnseResult(HttpServletRequest request) throws IOException, DocumentException;

}

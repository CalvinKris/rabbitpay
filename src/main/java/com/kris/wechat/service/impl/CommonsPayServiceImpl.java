package com.kris.wechat.service.impl;

import java.io.IOException;
import java.io.InputStream;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import org.jdom2.JDOMException;

import com.kris.wechat.service.CommonsPayService;
import com.kris.wechat.utils.MD5Utils;
import com.kris.wechat.utils.XMLUtil;

/**
 * 功能描述    :
 * 业务描述    :
 * 作 者 名    :@Kris
 * 开发日期    :2018/9/27 14:32
 * Created     :IntelliJ IDEA
 * 修改日期    :
 * 修 改 者    :
 * 修改内容    :
 *
 */
public class CommonsPayServiceImpl implements CommonsPayService {

    /*
     * 创建MD5摘要，按字典序排序，遇见空值的不参加签名，生成预请求XML
     * @param sortedMap 用户组装参数，微信发起预支付的参数按照所需封装于此
     * @param key  商户微信支付密钥
     */
    @Override
    public String CreateSign(SortedMap<String, String> sortedMap, String key) {
        Set           es = sortedMap.entrySet();
        Iterator      it = es.iterator();
        StringBuilder sb = new StringBuilder();

        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String    k     = (String) entry.getKey();
            String    v     = (String) entry.getValue();

            if ((null != v) &&!"".equals(v) &&!"sign".equals(k) &&!"key".equals(k)) {
                sb.append(k + "=" + v + "&");
            }
        }

        sb.append("key=" + key);

        String sign = MD5Utils.MD5Encode(sb.toString(), "UTF-8").toUpperCase();

        sortedMap.put("sign", sign);

        StringBuilder xml = new StringBuilder();

        xml.append("<xml>\n");

        for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
            if (!entry.getKey().equals("sign")) {
                xml.append("<" + entry.getKey() + "><![CDATA[")
                   .append(entry.getValue())
                   .append("]]></" + entry.getKey() + ">\n");
            } else {
                xml.append("<" + entry.getKey() + ">").append(entry.getValue()).append("</" + entry.getKey() + ">\n");
            }
        }

        xml.append("</xml>");

        String req = xml.toString();

        return req;
    }

    /*
     * 提交封装好的XML，向微信发起请求提交。并返回响应结果
     * @param req  HttpServletRequest
     * @param url   微信支付请求地址
     */
    @Override
    public Map<String, String> postExecute(String req, String url) {
        HttpClientBuilder   httpClientBuilder   = HttpClientBuilder.create();
        CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
        HttpPost            httpPost            = new HttpPost(url);
        StringEntity        entity;
        Map<String, String> map = null;

        httpPost.setEntity(new StringEntity(req, "UTF-8"));

        try {
            HttpResponse httpResponse = closeableHttpClient.execute(httpPost);
            HttpEntity   httpEntity   = httpResponse.getEntity();

            if (httpEntity != null) {
                String result = EntityUtils.toString(httpEntity, "UTF-8");

                result = result.replaceAll("<![CDATA[|]]>", "");
                map    = XMLUtil.doXMLParse(result);

                return map;
            }

            closeableHttpClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        }

        return map;
    }
    /*
    * 微信支付响应处理方法
    * return  格式化微信的响应结果
    * */
    @Override
    public Map<String, String> resopnseResult(HttpServletRequest request) throws IOException, DocumentException {
        Map<String, String> map = new HashMap<>();

        /* 从request中获取输入流 */
        InputStream inputStream = request.getInputStream();

        /* 读取输入流 */
        SAXReader saxReader = new SAXReader();
        Document  document  = saxReader.read(inputStream);

        /* 得到xml根元素 */
        Element root = document.getRootElement();

        /* 得到根元素所有子节点 */
        List<Element> elementList = root.elements();

        /* 遍历所有子节点 */
        for (Element e : elementList) {
            map.put(e.getName(), e.getText());
        }

        /* 释放资源 */
        inputStream.close();
        inputStream = null;

        return map;
    }
}



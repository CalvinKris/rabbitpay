package com.kris.wechat.service.impl;

import java.util.Map;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;

import com.kris.wechat.WeChatConstant;
import com.kris.wechat.service.CommonsPayService;
import com.kris.wechat.service.PayCoreService;
import com.kris.wechat.utils.CertHttpUtil;
import com.kris.wechat.utils.MD5Utils;

/**
 * 功能描述    :
 * 业务描述    :
 * 作 者 名    :@Kris
 * 开发日期    :2018/9/29 11:45
 * Created     :IntelliJ IDEA
 * 修改日期    :
 * 修 改 者    :
 * 修改内容    :
 *
 */
public class PayCoreServiceImpl implements PayCoreService {
    /**
     * Method description
     *
     *
     * @param sortedMap  用户组装参数，微信发起预支付的参数按照所需封装于此
     * @param key        商户微信支付密钥
     * @param request    HttpServletRequest
     *
     * @return            微信预支付生成结果 用户按照前端格式即可
     */
    @Override
    public Map<String, String> CreatePrePay(SortedMap<String, String> sortedMap, String key,
                                            HttpServletRequest request) {
        CommonsPayService payService = new CommonsPayServiceImpl();

        return payService.postExecute(payService.CreateSign(sortedMap, key), WeChatConstant.createPayUrl);
    }

    /**
     * Method description
     *
     * @param request    HttpServletRequest
     * @param sortedMap  用户组装参数，微信发起退款的参数按照所需封装于此
     * @param key        商户微信支付密钥
     *
     *
     * @return            微信退款响应结果
     */
    public String payRefund(HttpServletRequest request,SortedMap<String,String> sortedMap,String key)
            throws Exception {
        CommonsPayService payService=new CommonsPayServiceImpl();
        String req=payService.CreateSign(sortedMap,key);

        String resXml = CertHttpUtil.postData(WeChatConstant.refundUrl, req);
        return resXml;
    }
}


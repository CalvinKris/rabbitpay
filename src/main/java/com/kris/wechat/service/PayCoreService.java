package com.kris.wechat.service;

import java.util.Map;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述    :
 * 业务描述    :
 * 作 者 名    :@Kris
 * 开发日期    :2018/9/28 15:25
 * Created     :IntelliJ IDEA
 * 修改日期    :
 * 修 改 者    :
 * 修改内容    :
 *
 */
public interface PayCoreService {

    /**
     * Method description
     *
     *
     * @param sortedMap  用户组装参数，微信发起预支付的参数按照所需封装于此
     * @param key        商户微信支付密钥
     * @param request    HttpServletRequest
     *
     * @return            微信预支付生成结果 用户按照前端格式即可
     */
    Map<String, String> CreatePrePay(SortedMap<String, String> sortedMap, String key, HttpServletRequest request);

    /**
     * Method description
     *
     * @param request    HttpServletRequest
     * @param sortedMap  用户组装参数，微信发起退款的参数按照所需封装于此
     * @param key        商户微信支付密钥
     *
     *
     * @return            微信退款响应结果
     */
     String payRefund(HttpServletRequest request,SortedMap<String,String> sortedMap,String key) throws Exception;
}



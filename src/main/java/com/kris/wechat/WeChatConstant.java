package com.kris.wechat;

/**
 * 功能描述    :
 * 业务描述    :微信支付相关常量
 * 作 者 名    :@Kris
 * 开发日期    :2018/9/27 14:53
 * Created     :IntelliJ IDEA
 * 修改日期    :
 * 修 改 者    :
 * 修改内容    :
 * **************************************************************
 */
public class WeChatConstant {
    /*微信退款*/
    public static String refundUrl="https://api.mch.weixin.qq.com/secapi/pay/refund";
    /*公众号支付，小程序支付*/
    public static String createPayUrl="https://api.mch.weixin.qq.com/pay/unifiedorder";
    /*退款查询*/
    public static String refundQueryUrl="https://api.mch.weixin.qq.com/pay/refundquery";
    /*订单查询*/
    public static String orderQueryUrl="https://api.mch.weixin.qq.com/pay/orderquery";
    /*下载账单*/
    public static String downloadBillUrl="https://api.mch.weixin.qq.com/pay/downloadbill";
    public static String downloadFundFlow="https://api.mch.weixin.qq.com/pay/downloadfundflow";


}

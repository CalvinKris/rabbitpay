package com.kris.wechat.utils;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * 功能描述    :
 * 业务描述    :微信退款回调AES解密
 * 作 者 名    :@Kris
 * 开发日期    :2018/9/26 14:13
 * Created     :IntelliJ IDEA
 * 修改日期    :
 * 修 改 者    :
 * 修改内容    :
 * **************************************************************
 */
public class AESUtil {

    /**
     * 密钥算法
     */
    private static final String ALGORITHM = "AES";
    /**
     * 加解密算法/工作模式/填充方式
     */
    private static final String ALGORITHM_MODE_PADDING = "AES/ECB/PKCS7Padding";
    /**
     * 生成key
     */
    private static final String key ="WxpayConfig.key";
    private static SecretKeySpec secretKey = new SecretKeySpec(MD5Utils.MD5Encode(key, "UTF-8").toLowerCase().getBytes(), ALGORITHM);

    /**
     * AES加密
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String encryptData(String data) throws Exception {
        // 创建密码器
        Cipher cipher = Cipher.getInstance(ALGORITHM_MODE_PADDING);        // 初始化
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64Util.encode(cipher.doFinal(data.getBytes()));
    }

    /**
     * AES解密
     */
    public static String decryptData(String base64Data) throws Exception {
        // 创建密码器
        Cipher cipher = Cipher.getInstance(ALGORITHM_MODE_PADDING);
        // 初始化
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64Util.decode(base64Data)));
    }


}
